﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace UnitTests
{
    /// <summary>
    /// Utility fucntions to access protected and private instance methods, through the use of reflections
    /// </summary>
    public class Common
    {

        /// <summary>
        /// Calls the private  or protected method of an instance according to the specified binding flags 
        /// </summary>
        /// <param name="t"></param>
        /// <param name="strMethod"></param>
        /// <param name="objInstance"></param>
        /// <param name="objParams"></param>
        /// <param name="eFlags"></param>
        /// <returns></returns>
        private static Object RunMethod(System.Type t, string strMethod, object objInstance, object[] objParams, BindingFlags eFlags)
        {
            MethodInfo m;
            try
            {
                m = t.GetMethod(strMethod, eFlags);
                if (m == null)
                {
                    throw new ArgumentException("There is no method '" +
                     strMethod + "' for type '" + t.ToString() + "'.");
                }

                object objRet = m.Invoke(objInstance, objParams);
                return objRet;
            }
            catch
            {
                throw;
            }

        }

        /// <summary>
        /// Calls the RunMethod function with the respective binding flags
        /// </summary>
        /// <param name="t"></param>
        /// <param name="strMethod"></param>
        /// <param name="objInstance"></param>
        /// <param name="aobjParams"></param>
        /// <returns></returns>
        public static object RunInstanceMethod(System.Type t, string strMethod, object objInstance, object[] aobjParams)
        {
            BindingFlags eFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            return RunMethod(t, strMethod, objInstance, aobjParams, eFlags);
        }
    }
}
