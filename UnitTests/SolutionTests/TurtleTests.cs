using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

using TurtleSolution.SolutionObjects;
using TurtleSolution.Common;
using TurtleSolution.Interfaces;

using Rhino.Mocks;

namespace UnitTests.SolutionTests
{
    [TestClass]
    public class TurtleTests
    {
        /// <summary>
        /// This unit test checks that the initialisation processes puts the Turtle object in the appropriate state
        /// </summary>
        [TestMethod]
        public void CheckTurtleInitialisation()
        {
            Tuple<int, int> boardSize = new Tuple<int, int>(5,5);
            LinkedList<Tuple<int, int>> mines = new LinkedList<Tuple<int, int>>();
            Tuple<int, int> exitLocation = new Tuple<int, int>(5,5);
            
            var stubWorld = MockRepository.GenerateStub<IWorld>();

            LinkedList<Queue<DataTypes.Command>> paths = new LinkedList<Queue<DataTypes.Command>>();

            Tuple<int, int> initialPosition = new Tuple<int, int>(1, 1);
            TurtleSolution.Common.DataTypes.Direction initalDirection = DataTypes.Direction.N;

            Turtle testTurtle = new Turtle(initialPosition, initalDirection, (stubWorld as World), paths);

            Assert.AreEqual(testTurtle.OriginalPosX, initialPosition.Item1);
            Assert.AreEqual(testTurtle.OriginalPosY, initialPosition.Item2);
            Assert.AreEqual(testTurtle.OriginalDirection, initalDirection);
        }

        /// <summary>
        /// Shortcut method that issues a command  to the private method of the turtle class 'CommandTurtle'
        /// </summary>
        /// <param name="turtleInstance"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        public static DataTypes.CommandResult CommandTurtle(Turtle turtleInstance, DataTypes.Command command)
        {
            object obj = Common.RunInstanceMethod(typeof(Turtle), "CommandTurtle", turtleInstance, new object[] { command });
            return (DataTypes.CommandResult)obj;
        }


        /// <summary>
        /// This unit test checks that the movement of the turtle is sane
        /// It also checks that the commandresult returned for a particular WorldNode type is the same as the expected result
        /// </summary>
        [TestMethod]
        public void CheckTurtleMovement()
        {
            Tuple<int, int> boardSize = new Tuple<int, int>(5, 5);
            LinkedList<Tuple<int, int>> mines = new LinkedList<Tuple<int, int>>();
            Tuple<int, int> exitLocation = new Tuple<int, int>(5, 5);

            WorldNode topNode = new WorldNode(NodeType.EXIT, 1);
            WorldNode botNode = new WorldNode(NodeType.MINE, 2);
            WorldNode leftNode = new WorldNode(NodeType.NORM, 3);
            WorldNode rightNode = new WorldNode(NodeType.NORM, 4);

            //mocking the world and setting the return statmenet of the GetNode method
            var stubWorld = MockRepository.GenerateMock<IWorld>();
            stubWorld.Stub(x => x.GetNode(1,2)).Return(botNode);
            stubWorld.Stub(x => x.GetNode(1,0)).Return(topNode);
            stubWorld.Stub(x => x.GetNode(0,1)).Return(leftNode);
            stubWorld.Stub(x => x.GetNode(2,1)).Return(rightNode);

            LinkedList<Queue<DataTypes.Command>> paths = new LinkedList<Queue<DataTypes.Command>>();
            Tuple<int, int> initialPosition = new Tuple<int, int>(1, 1);
            TurtleSolution.Common.DataTypes.Direction initalDirection = DataTypes.Direction.N;

            Turtle testTurtle = new Turtle(initialPosition, initalDirection,stubWorld , paths);

            //asserting top node is the exit node 
            DataTypes.CommandResult result = CommandTurtle(testTurtle, DataTypes.Command.MOVE);
            Assert.AreEqual(DataTypes.CommandResult.FOUND_EXIT, result);

            object obj = Common.RunInstanceMethod(typeof(Turtle), "ResetTurtle", testTurtle, null);

            //rotating left 
            result = CommandTurtle(testTurtle, DataTypes.Command.ROTATE_LEFT);
            Assert.AreEqual(DataTypes.CommandResult.ROTATION, result);

            // Check if turtle is facing east 
            Assert.AreEqual(testTurtle.CurrentDirection, DataTypes.Direction.W);

            // move east
            result = CommandTurtle(testTurtle, DataTypes.Command.MOVE);
            Assert.AreEqual(DataTypes.CommandResult.MOVE_OK, result);

            //reset turtule status
            obj = Common.RunInstanceMethod(typeof(Turtle), "ResetTurtle", testTurtle, null);

            //rotating left
            result = CommandTurtle(testTurtle, DataTypes.Command.ROTATE_LEFT);
            Assert.AreEqual(DataTypes.CommandResult.ROTATION, result);

            //rotating left
            result = CommandTurtle(testTurtle, DataTypes.Command.ROTATE_LEFT);
            Assert.AreEqual(DataTypes.CommandResult.ROTATION, result);

            // Check if the turtle is facign south
            Assert.AreEqual(testTurtle.CurrentDirection, DataTypes.Direction.S);

            // check if the result is a mine hit
            result = CommandTurtle(testTurtle, DataTypes.Command.MOVE);
            Assert.AreEqual(DataTypes.CommandResult.HIT_MINE, result);

            obj = Common.RunInstanceMethod(typeof(Turtle), "ResetTurtle", testTurtle, null);

            result = CommandTurtle(testTurtle, DataTypes.Command.ROTATE_RIGHT);
            Assert.AreEqual(testTurtle.CurrentDirection, DataTypes.Direction.E);
            //move west
            result = CommandTurtle(testTurtle, DataTypes.Command.MOVE);
            Assert.AreEqual(DataTypes.CommandResult.MOVE_OK, result);
        }


        /// <summary>
        /// This unit test checks that the private method ResetTurtle executes as expected and actually 
        /// gets the turtle into the initial position
        /// </summary>
        [TestMethod]
        public void CheckResetBehaviour()
        {
            Tuple<int, int> boardSize = new Tuple<int, int>(5, 5);
            LinkedList<Tuple<int, int>> mines = new LinkedList<Tuple<int, int>>();
            Tuple<int, int> exitLocation = new Tuple<int, int>(5, 5);

            WorldNode node = new WorldNode(NodeType.EXIT, 1);
            var stubWorld = MockRepository.GenerateStub<IWorld>();
            stubWorld.Stub(x => x.GetNode(1, 0)).Return(node);

            LinkedList<Queue<DataTypes.Command>> paths = new LinkedList<Queue<DataTypes.Command>>();
            Tuple<int, int> initialPosition = new Tuple<int, int>(1, 1);
            TurtleSolution.Common.DataTypes.Direction initalDirection = DataTypes.Direction.N;

            Turtle testTurtle = new Turtle(initialPosition, initalDirection, stubWorld, paths);

            CommandTurtle(testTurtle, DataTypes.Command.MOVE);

            Assert.AreEqual(testTurtle.OriginalPosX, testTurtle.CurrentPosX);
            Assert.AreNotEqual(testTurtle.OriginalPosY, testTurtle.CurrentPosY);

            //reset turtule status
            object obj = Common.RunInstanceMethod(typeof(Turtle), "ResetTurtle", testTurtle, null);
            
            // check if the currentpos is back equal to originalpos
            Assert.AreEqual(testTurtle.OriginalPosX, testTurtle.CurrentPosX);
            Assert.AreEqual(testTurtle.OriginalPosY, testTurtle.CurrentPosY);
        }
    }

}