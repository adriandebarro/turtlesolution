﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TurtleSolution.Common;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests.SolutionTests
{
    /// <summary>
    /// This Test class contains unit tests for utility methods found under the Common
    /// </summary>
    [TestClass]
    public class UtilsTests
    { 
        /// <summary>
        /// Checks the behaviour of the command converter
        /// </summary>
        [TestMethod]
        public void CheckConvertCommandUtil()
        {
            //intialise actual result
            DataTypes.Command actualResult = DataTypes.Command.UNDEFINED;
            //init the first test for a right rotation
            string inputCommand = DataTypes.ROTATE_RIGHT_REP.ToString();
            DataTypes.Command expectedCommand = DataTypes.Command.ROTATE_RIGHT;
            actualResult = Utils.ConvertCommand(inputCommand);
            //assert that the output is equal to the rotate right command
            Assert.AreEqual(expectedCommand, actualResult);

            // init test for a left rotation
            inputCommand = DataTypes.ROTATE_LEFT_REP.ToString();
            expectedCommand = DataTypes.Command.ROTATE_LEFT;
            actualResult = Utils.ConvertCommand(inputCommand);
            //assert outcome is a a left rotation command
            Assert.AreEqual(expectedCommand, actualResult);

            // init test for a move 
            inputCommand = DataTypes.MOVE_REP.ToString();
            expectedCommand = DataTypes.Command.MOVE;
            actualResult = Utils.ConvertCommand(inputCommand);
            //assert outcome is the move command
            Assert.AreEqual(expectedCommand, actualResult);

            // init test for an unkown command
            inputCommand = "Z";
            expectedCommand = DataTypes.Command.UNDEFINED;
            actualResult = Utils.ConvertCommand(inputCommand);
            // assert the outcome is the command undefined
            Assert.AreEqual(expectedCommand, actualResult);
        }

        /// <summary>
        /// Checks the behaviour of the ParseDirection util 
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CheckParseDirectionUtil()
        { 
            // init test for the up direction
            char charDirection = DataTypes.UP_REP;
            DataTypes.Direction expectedDirection = DataTypes.Direction.N;
            DataTypes.Direction actualDirection;
            actualDirection =  Utils.ParseDirection(charDirection);
            // assert the direction is a the up direction
            Assert.AreEqual(expectedDirection, actualDirection);

            // init test for the right direction
            charDirection = DataTypes.RIGHT_REP;
            actualDirection = Utils.ParseDirection(charDirection);
            expectedDirection = DataTypes.Direction.E;
            // assert the direction 
            Assert.AreEqual(expectedDirection, actualDirection);

            // init test for the down direction
            charDirection = DataTypes.DOWN_REP;
            actualDirection = Utils.ParseDirection(charDirection);
            expectedDirection = DataTypes.Direction.S;
            Assert.AreEqual(expectedDirection, actualDirection);

            // init test for the down direction
            charDirection = DataTypes.LEFT_REP;
            actualDirection = Utils.ParseDirection(charDirection);
            expectedDirection = DataTypes.Direction.W;
            Assert.AreEqual(expectedDirection, actualDirection);

            // init test for an undefined direction which will result in an ArgumentException
            charDirection = 'F';
            actualDirection = Utils.ParseDirection(charDirection);
        }

        /// <summary>
        /// Checks the ProcessPath util function for its defined behaviour
        /// </summary>
        [TestMethod]
        public void CheckProcessPathsUtil()
        {
            // init test with a defined sequence of commands
            LinkedList<LinkedList<string>> inputList = new LinkedList<LinkedList<string>>();
            LinkedList<string> path = new LinkedList<string>();
            path.AddLast(DataTypes.MOVE_REP.ToString());
            path.AddLast(DataTypes.MOVE_REP.ToString());
            path.AddLast(DataTypes.MOVE_REP.ToString());
            path.AddLast(DataTypes.ROTATE_LEFT_REP.ToString());
            inputList.AddLast(path);
            LinkedList<Queue<DataTypes.Command>> actualResult =  Utils.ProcessPaths(inputList);
            //expected count of paths is equal to the amount of inputted paths
            int expectedResult = inputList.Count;
            // assert that there is only one path
            Assert.AreEqual(actualResult.Count, expectedResult);

            //expected count of commands in the path sequence is equal to the inputted sequence
            expectedResult = path.Count;
            // assert the expectation
            Assert.AreEqual(actualResult.ElementAt(0).Count, expectedResult);

        }

        /// <summary>
        /// Checks the ProcessPath for an undefined behaviour, the method will result in an argument exception
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CheckProcessPathUtilBadPath()
        {
            LinkedList<LinkedList<string>> inputList = new LinkedList<LinkedList<string>>();
            LinkedList<string> path = new LinkedList<string>();
            // add an undefined command
            path.AddLast("A");
            inputList.AddLast(path);
            LinkedList<Queue<DataTypes.Command>> actualResult = Utils.ProcessPaths(inputList);
        }
    }
}
