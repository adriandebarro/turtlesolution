﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TurtleSolution.SolutionObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests.SolutionObjectsTests
{
    [TestClass]
    public class WorldNodeTests
    {
        /// <summary>
        /// Asserting the initialisation behaviour of the WorldNode
        /// </summary>
        [TestMethod]
        public void CheckWorldNodeInitialisation()
        {
            WorldNode testNode = new WorldNode(NodeType.NORM, 0);

            Assert.AreEqual(testNode.Type, NodeType.NORM);
            Assert.AreEqual(0, testNode.NodeID);
        }
    }
}
