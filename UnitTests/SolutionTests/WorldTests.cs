﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using TurtleSolution.SolutionObjects;


namespace UnitTests.SolutionTests
{
    [TestClass]
    public class WorldTests
    {
        /// <summary>
        /// Method that builds a shell world instance for testing
        /// </summary>
        /// <returns></returns>
        public World Setup()
        {
            Tuple<int, int> boardSize = new Tuple<int, int>(5,5);
            LinkedList<Tuple<int, int>> mines = new LinkedList<Tuple<int, int>>();
            Tuple<int, int> exitPoint = new Tuple<int, int>(2,2);

            return new World(boardSize, mines, exitPoint);
        }

        /// <summary>
        /// Method that builds a world instances with specified amount of mines and an Exit point
        /// </summary>
        /// <returns></returns>
        public World SetupWithMines()
        {
            Tuple<int, int> boardSize = new Tuple<int, int>(5, 5);
            LinkedList<Tuple<int, int>> mines = new LinkedList<Tuple<int, int>>();
            mines.AddLast(new Tuple<int, int>(2,3));
            mines.AddLast(new Tuple<int, int>(3,3));
            Tuple<int, int> exitPoint = new Tuple<int, int>(1, 1);

            return new World(boardSize, mines, exitPoint);
        }

        /// <summary>
        /// Checks the intialisation procedure of the world
        /// </summary>
        [TestMethod]
        public void CheckWorldInitialisation()
        {
            World testWorld = Setup();
            testWorld.InitialiseWorld();
            Tuple<int, int> expectedBoardSize = new Tuple<int, int>(5,5);

            //check that the set configuration is equal to what is expected
            Assert.AreEqual(testWorld.BoardSize, expectedBoardSize);

            //setup world
            testWorld.InitialiseWorld();

            int expectedNumberNodes = expectedBoardSize.Item1 * expectedBoardSize.Item2;
            //confirm that the amount of nodes in the world is as expected
            Assert.AreEqual(testWorld.WorldNodes.Length,expectedNumberNodes);
        }

        /// <summary>
        /// checks that the world behaves as it should, i.e., it retruns the respective nodes
        /// </summary>
        [TestMethod]
        public void CheckRetreivalOfWorldNodes()
        {
            World testWorld = SetupWithMines();
            testWorld.InitialiseWorld();

            NodeType expectedNodeType = NodeType.EXIT;
            int expectedNodeID = testWorld.BoardSize.Item1 * 1 + 1;
            WorldNode resultingNode = testWorld.GetNode(1, 1);

            //asserting that the retreived node is the same
            Assert.AreEqual(resultingNode.Type, expectedNodeType);
            //asserting that the node has the specified id
            Assert.AreEqual(resultingNode.NodeID, expectedNodeID);

            //asserting that retreived node is a mine
            resultingNode = testWorld.GetNode(3,3);
            expectedNodeType = NodeType.MINE;
            Assert.AreEqual(resultingNode.Type, expectedNodeType);

            //asserting retreived node is a mine
            resultingNode = testWorld.GetNode(2,3);
            Assert.AreEqual(resultingNode.Type, expectedNodeType);

            //asserting the retreived node is a normal node
            resultingNode = testWorld.GetNode(2,2);
            expectedNodeType = NodeType.NORM;
            Assert.AreEqual(resultingNode.Type, expectedNodeType);
        }
    }
}
