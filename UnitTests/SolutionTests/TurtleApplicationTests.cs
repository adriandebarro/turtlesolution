﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using TurtleSolution.SolutionObjects;
using TurtleSolution.Common;


namespace UnitTests.SolutionObjectsTests
{
    [TestClass]
    public class TurtleApplicationTests
    {


        /// <summary>
        /// Testing that the parsing fires an exception of the same type
        /// </summary>
        [TestMethod]
        public void CheckCommandLineParsingException()
        {
            try
            {
                string[] testArgs = new string[1];
                testArgs[0] = "this is a test";
                TurtleApplication turtleApplication = new TurtleApplication(testArgs);
                object output = Common.RunInstanceMethod(typeof(TurtleApplication), "ParseCommandLineArguments", turtleApplication, new object[] { testArgs });
            }
            catch(Exception ex)
            {
                //Assert.IsInstanceOfType(ex.InnerException.GetType(), expectedException.GetType());
                //asserting the received exception
                FileNotFoundException expectedException = new FileNotFoundException();
                Exception innerException = ex.InnerException;
                Assert.AreEqual(innerException.Message, "Inputted filepath is not properly set");
            }
        }

        /// <summary>
        /// Checking if the behaviour of the commandline parsing
        /// </summary>
        [TestMethod]
        public void CheckCommandLineParsing()
        {
            //init with a good inputfile
            string[] testArgs = new string[1];
            testArgs[0] = "../../input/input.txt";
            TurtleApplication turtleApplication = new TurtleApplication(testArgs);
            object output = Common.RunInstanceMethod(typeof(TurtleApplication), "ParseCommandLineArguments", turtleApplication, new object[] { testArgs });
            bool actualResult = (bool)output;
            bool expectedResult = true;
            // asserting that the commandlineparser parsed succesfully the inputted arguments
            Assert.AreEqual(actualResult, expectedResult);
        }

        /// <summary>
        /// Checking that the ExitLocation parser executes as expected
        /// </summary>
        [TestMethod]
        public void CheckProcessExitLocation()
        {
            string[] testArgs = new string[1];
            testArgs[0] = "../../input/input.txt";
            TurtleApplication turtleApplication = new TurtleApplication(testArgs);
            LinkedList<string> testInput = new LinkedList<string>();
            testInput.AddLast("5");
            testInput.AddLast("4");
            object output = Common.RunInstanceMethod(typeof(TurtleApplication), "ProcessExitLocation", turtleApplication, new object[] { testInput });

            Tuple<int, int> actualOutput = (Tuple<int, int>)output;
            Tuple<int, int> expectedOutput = new Tuple<int, int>(5,4);

            Assert.AreEqual(expectedOutput, actualOutput);
        }

        /// <summary>
        /// Checking that the BoardDimensions parses executes as expected
        /// </summary>
        [TestMethod]
        public void CheckBordDimensionParser()
        {
            string[] testArgs = new string[1];
            testArgs[0] = "../../input/input.txt";
            TurtleApplication turtleApplication = new TurtleApplication(testArgs);
            LinkedList<string> testInput = new LinkedList<string>();
            testInput.AddLast("5");
            testInput.AddLast("4");
            object output = Common.RunInstanceMethod(typeof(TurtleApplication), "ProcessBoardDimensions", turtleApplication, new object[] { testInput });

            Tuple<int, int> actualOutput = (Tuple<int, int>)output;
            Tuple<int, int> expectedOutput = new Tuple<int, int>(5, 4);

            Assert.AreEqual(expectedOutput, actualOutput);
        }

        /// <summary>
        /// Checking that the MineProcessor executes as expected
        /// </summary>
        [TestMethod]
        public void CheckMineProcessor()
        {
            string[] testArgs = new string[1];
            testArgs[0] = "../../input/input.txt";
            TurtleApplication turtleApplication = new TurtleApplication(testArgs);

            LinkedList<string> testInput = new LinkedList<string>();
            testInput.AddLast("1,1");
            testInput.AddLast("2,2");
            object output = Common.RunInstanceMethod(typeof(TurtleApplication), "ProcessMines", turtleApplication, new object[] { testInput });

           LinkedList<Tuple<int,int>> actualOutput = (LinkedList<Tuple<int, int>>) output;
            Tuple<int, int> expectedOutput1 = new Tuple<int, int>(1,1);
            Tuple<int, int> expectedOutput2 = new Tuple<int, int>(2,2);

            int expectedCount = 2;

            Assert.AreEqual(expectedCount, actualOutput.Count);
            Assert.AreEqual(expectedOutput1, actualOutput.ElementAt(0));
            Assert.AreEqual(expectedOutput2, actualOutput.ElementAt(1));
        }
    }
}
