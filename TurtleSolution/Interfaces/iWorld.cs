﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TurtleSolution.SolutionObjects;

namespace TurtleSolution.Interfaces
{
    /// <summary>
    /// Interface for the world
    /// </summary>
    public interface IWorld
    {
        Tuple<int, int> BoardSize { get; }
        LinkedList<Tuple<int, int>> Mines { get; }
        Tuple<int, int> Exit { get; }
        WorldNode[] WorldNodes { get; }
        void InitialiseWorld();
        WorldNode GetNode(int x, int y);
    }
}
