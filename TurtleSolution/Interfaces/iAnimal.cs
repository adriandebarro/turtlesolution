﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurtleSolution.Common;

namespace TurtleSolution.SolutionObjects
{
    /// <summary>
    /// Interface for any animal that would be interacting with the world
    /// </summary>
    public interface IAnimal
    {
        DataTypes.Direction CurrentDirection { get; }
        WorldNode CurrentNode { get; }

        int OriginalPosX { get; }
        int OriginalPosY { get; }

        int CurrentPosX { get; }
        int CurrentPosY { get; }

        DataTypes.Direction OriginalDirection { get; }

        void InitialiseTurtle();
        void CheckPaths();
    }
}
