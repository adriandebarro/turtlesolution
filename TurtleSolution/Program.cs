﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TurtleSolution.SolutionObjects;
namespace TurtleSolution
{
    class Program
    {
        /// <summary>
        /// Entry point to the turtle application
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            TurtleApplication turtleApplication = new TurtleApplication(args);
            turtleApplication.Run();
        }
    }
}
