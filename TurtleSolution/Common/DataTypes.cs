﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurtleSolution.Common
{
    /// <summary>
    /// This class contains DataTypes required by the application, which
    /// are used by all the different classes in the application
    /// </summary>
    public class DataTypes
    {
        /// <summary>
        /// Enum representing the possible directions 
        /// </summary>
        public enum Direction
        {
            N,
            E,
            W,
            S
        }

        /// <summary>
        /// Acceptable commands by the solution
        /// </summary>
        public enum Command
        {
            ROTATE_LEFT,
            ROTATE_RIGHT,
            MOVE,
            UNDEFINED
        }

        /// <summary>
        /// Outcome of a command
        /// </summary>
        public enum CommandResult
        {
            HIT_MINE,
            FOUND_EXIT,
            MOVE_OK,
            ROTATION,
            INITIATED,
            ERROR
        }

        public const char ROTATE_RIGHT_REP = 'R';
        public const char ROTATE_LEFT_REP = 'L';
        public const char MOVE_REP = 'M';

        public const char UP_REP = 'N';
        public const char DOWN_REP = 'S';
        public const char LEFT_REP = 'W';
        public const char RIGHT_REP = 'E';

    }
}
