﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurtleSolution.Common
{
    /// <summary>
    /// The Application absract class provides a number of functions that need to be overriden 
    /// </summary>
    public abstract class Application
    {
        private string[] CommandLineArguments;

        /// <summary>
        /// Initialisation of input
        /// </summary>
        protected abstract void Initialise();
        
        /// <summary>
        /// Execute the actual application
        /// </summary>
        protected abstract void Execute();
        
        /// <summary>
        /// Parse any required commandline arguments
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        protected abstract bool ParseCommandLineArguments(string[] args);

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="commandLineArgs">commandline input</param>
        public Application(string[] commandLineArgs)
        {
            CommandLineArguments = commandLineArgs;
        }

        /// <summary>
        /// The run method is the only method that is externally visible 
        /// and it provides the means to interact with the application
        /// </summary>
        public void Run()
        {
            try
            {
                Common.Utils.PrintDebugInformation("Starting Application");
                bool succesfulParsing = ParseCommandLineArguments(CommandLineArguments);
                if (succesfulParsing)
                {
                    Initialise();
                    Execute();
                    Terminate();
                }
                else
                {
                    Common.Utils.PrintToScreen("Application interrupted");
                    Common.Utils.Pause();
                }
            }
            catch (Exception ex)
            {
                Common.Utils.PrintToScreen(ex.Message);
                Common.Utils.PrintDebugInformation("Application exiting now.");
                Common.Utils.Pause();
            }
        }

        /// <summary>
        /// This method allows the releasing of any acquired services 
        /// </summary>
        protected virtual void Terminate()
        {
            Common.Utils.PrintDebugInformation("Exiting application");
            Common.Utils.Pause();
        }
    }
}
