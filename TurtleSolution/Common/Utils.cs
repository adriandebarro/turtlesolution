﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;
using System.IO;


using Direction = TurtleSolution.Common.DataTypes.Direction;
using Command = TurtleSolution.Common.DataTypes.Command;
using CommandResult = TurtleSolution.Common.DataTypes.CommandResult;
using System.Diagnostics;

namespace TurtleSolution.Common
{

    /// <summary>
    /// Class conatining common utilities that may be used by more than on class 
    /// </summary>
    public class Utils
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static LinkedList<LinkedList<string>> ReadFile(string filePath)
        {
            LinkedList<LinkedList<string>>  fileLines = new LinkedList<LinkedList<string>>();
            using (TextFieldParser csvReader = new TextFieldParser(filePath))
            {
                LinkedList<string> currentLinkedList = new LinkedList<string>();
                csvReader.SetDelimiters(new string[] { " " });
                csvReader.HasFieldsEnclosedInQuotes = true;
                string currentLine = null;
                    
                while ((currentLine = csvReader.ReadLine()) != null)
                {
                    string[] pieces = currentLine.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                        
                    foreach (string currentPiece in pieces)
                    {
                        currentLinkedList.AddLast(currentPiece);
                    }

                    fileLines.AddLast(new LinkedList<string>(currentLinkedList));
                    currentLinkedList.Clear();
                }   
            }

            return fileLines;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void Pause()
        {
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stringifiedFirstComponent"></param>
        /// <param name="stringifiedSecondComponent"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static Tuple<int, int> ParseIntoIntTuple(string stringifiedFirstComponent, string stringifiedSecondComponent, string errorMessage)
        {
            int firstComponent = 0;
            int secondComponent = 0;
            if(int.TryParse(stringifiedFirstComponent, out firstComponent))
            {
                if(int.TryParse(stringifiedSecondComponent, out secondComponent))
                {
                    return new Tuple<int, int>(firstComponent, secondComponent);
                }
                else
                {
                    throw new Exception(errorMessage);
                }
            }
            else
            {
                throw new Exception(errorMessage);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public static Command ConvertCommand(string command)
        {
            char charCommand = ' ';
                
            if(char.TryParse(command, out charCommand))
            {
                switch(charCommand)
                {
                    case DataTypes.ROTATE_RIGHT_REP:
                        return Command.ROTATE_RIGHT;

                    case DataTypes.ROTATE_LEFT_REP:
                        return Command.ROTATE_LEFT;

                    case DataTypes.MOVE_REP:
                        return Command.MOVE;

                    default:
                        return Command.UNDEFINED;
                }
            }
            else
            {
                return Command.UNDEFINED;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stringifiedPaths"></param>
        /// <returns></returns>
        public static LinkedList<Queue<Command>> ProcessPaths(LinkedList<LinkedList<string>> stringifiedPaths)
        {
            LinkedList<Queue<Command>> processedPaths = new LinkedList<Queue<Command>>();
            foreach(LinkedList<string> currentPath in stringifiedPaths)
            {
                Queue<Command> currentlyProcessedPath = new Queue<Command>();
                foreach(string stringifiedCommand in currentPath)
                {
                    Command returnedCommand = ConvertCommand(stringifiedCommand);
                    if (returnedCommand != Command.UNDEFINED)
                        currentlyProcessedPath.Enqueue(returnedCommand);
                    else
                        throw new ArgumentException("Failed to process paths");
                }
                processedPaths.AddLast(new Queue<Command>(currentlyProcessedPath));
            }

            return processedPaths;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public static void PrintToScreen(string message)
        {
            Console.WriteLine(message);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        [ConditionalAttribute("DEBUG")]
        public static void PrintDebugInformation(string message)
        {
            Console.WriteLine(message);
        }

        /// <summary>
        /// Converts the char direction into the respective enum direction
        /// </summary>
        /// <param name="direction">Input direction from file</param>
        /// <returns>Enum representation of the inputted direction</returns>
        public static Direction ParseDirection(char direction)
        {
            switch (direction)
            {
                case DataTypes.UP_REP:
                    return Direction.N;
                case DataTypes.LEFT_REP:
                    return Direction.W;

                case DataTypes.RIGHT_REP:
                    return Direction.E;

                case DataTypes.DOWN_REP:
                    return Direction.S;

                default:
                    throw new ArgumentException("Failed to parse the initial turtle direction");
            }
        }
    }
}
