using System;
using System.Collections.Generic;
using TurtleSolution.Common;
using TurtleSolution.Interfaces;

///shortcut symbols
using Direction = TurtleSolution.Common.DataTypes.Direction;
using Command = TurtleSolution.Common.DataTypes.Command;
using CommandResult = TurtleSolution.Common.DataTypes.CommandResult;

namespace TurtleSolution.SolutionObjects
{
    /// <summary>
    /// The turtle object that interacts with the world object through the inputted paths
    /// </summary>
	public class Turtle : IAnimal
	{
        /// <summary>
        /// Messages to be shown accoridng to the outcome of path
        /// </summary>
        private const string HIT_MINE_MESSAGE = ":Mine hit!";
        private const string DIDNT_FIND_EXIT = ":Still in danger!";
        private const string FOUND_EXIT = ":Success!";


        private Direction _CurrentDirection;
        public Direction CurrentDirection
        {
            get { return _CurrentDirection; }
        }

        private WorldNode _CurrentNode;
        public WorldNode CurrentNode
        {
            get { return _CurrentNode; }
        }

        private int _OriginalPosX;
        public int OriginalPosX
        {
            get { return _OriginalPosX; }
        }

        /// <summary>
        /// Initial corrdinates for each 
        /// </summary>
        private int _OriginalPosY;
        public int OriginalPosY
        {
            get { return _OriginalPosY; }
        }

        private Direction _OriginalDirection;
        public Direction OriginalDirection
        {
            get { return _OriginalDirection; }
        }

        /// <summary>
        /// Current position in world
        /// </summary>
        private int _CurrentPosX;
        public int CurrentPosX
        {
            get { return _CurrentPosX; }
        }

        private int _CurrentPosY;
        public int CurrentPosY
        {
            get { return _CurrentPosY; }
        }


        /// <summary>
        /// All paths 
        /// </summary>
        private LinkedList<Queue<Command>> Paths;
        private IWorld TurtleWorld;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="initialPosition">Initial position of turtle in world</param>
        /// <param name="initialDirection">Initial direction of turtle in world</param>
        /// <param name="turtuleWorld">Initial direction of turtle in world</param>
        /// <param name="paths">All processed paths the turtle needs to check if viable</param>
        public Turtle(Tuple<int,int> initialPosition, Direction initialDirection, IWorld turtleWorld, LinkedList<Queue<Command>> paths)
		{
            this._CurrentDirection = Direction.N;
            this.Paths = paths;
            this.TurtleWorld = turtleWorld;
            this._OriginalPosX = initialPosition.Item1;
            this._OriginalPosY = initialPosition.Item2;
            this._CurrentPosX = this._OriginalPosX;
            this._CurrentPosY = this._OriginalPosY;
            this._OriginalDirection = initialDirection;
            this._CurrentDirection = this._OriginalDirection;
		}

        /// <summary>
        /// Initialises the turtle
        /// Gets the inital location of the turtle
        /// Makes sure the current position is set to the sup[posed inital position of the turtle
        /// </summary>
        public void InitialiseTurtle()
        {
            _CurrentNode = this.TurtleWorld.GetNode(_OriginalPosX, _OriginalPosY);
            Common.Utils.PrintDebugInformation("Turtle inital position is,"+_OriginalPosX+", "+_OriginalPosY);
            ResetTurtle();
        }

        /// <summary>
        /// Checks which received paths are a viable plan
        /// </summary>
        public void CheckPaths()
        {
            int index = 1;

            foreach(Queue<Command> currentPath in Paths)
            {
                CommandResult result = CommandResult.INITIATED;
                while (currentPath.Count > 0)
                {
                    Command currentCommand = currentPath.Dequeue();
                    result = CommandTurtle(currentCommand);
                    if (result == CommandResult.HIT_MINE)
                        break;
                }

                if (result == CommandResult.HIT_MINE)
                {
                    Common.Utils.PrintToScreen("Sequence " + index + HIT_MINE_MESSAGE);
                }
                else if(result == CommandResult.FOUND_EXIT)
                {
                    Common.Utils.PrintToScreen("Sequence " + index + FOUND_EXIT);
                }
                else
                {
                    Common.Utils.PrintToScreen("Sequence "+index+DIDNT_FIND_EXIT);
                }

                index++;
                ResetTurtle();
            }
        }

        /// <summary>
        /// Moves the turtle according to the direction to which it is facing
        /// </summary>
        /// <returns></returns>
        private CommandResult MoveTurtle()
        {
            switch(this._CurrentDirection)
            {
                //if facing north, move by y-axis up (-1)
                case Direction.N:
                    this._CurrentPosY -= 1;
                    break;
                
                //if facing west, move by x-axis left (-1)
                case Direction.W:
                    this._CurrentPosX -= 1;
                    break;
                
                //if facing east, move by x-axis right (+1)
                case Direction.E:
                    this._CurrentPosX += 1;
                    break;
                
                //if facing east, move by y-axis down(+1)
                case Direction.S:
                    this._CurrentPosY += 1;
                    break;
            }

            //get the node that the turtle is standing on
            _CurrentNode = this.TurtleWorld.GetNode(CurrentPosX, CurrentPosY);
            
            //report back with the result of the command
            switch(_CurrentNode.Type)
            {
                case NodeType.NORM:
                    return CommandResult.MOVE_OK;

                case NodeType.EXIT:
                    return CommandResult.FOUND_EXIT;

                case NodeType.MINE:
                    return CommandResult.HIT_MINE;

                default:
                    return CommandResult.ERROR;
            }
        }


        /// <summary>
        /// Move or rotate turtle
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        private CommandResult CommandTurtle(Command command)
        {
            switch(command)
            {
                case Command.MOVE:
                {
                    return MoveTurtle();
                }

                case Command.ROTATE_LEFT: case Command.ROTATE_RIGHT:
                {
                    RotateTurtle(command);
                    return CommandResult.ROTATION;
                }

                default:
                {
                    return CommandResult.ERROR;
                }
            }
        }

        /// <summary>
        /// Rotates turtle either left by 90 degrees or right by 90 degrees
        /// </summary>
        /// <param name="command">Rotation type</param>
        private void RotateTurtle(Command rotationType)
        {
            switch (rotationType)
            {
                case Command.ROTATE_LEFT:
                    RotateLeft();
                    break;

                case Command.ROTATE_RIGHT:
                    RotateRight();
                    break;
            }
        }

        /// <summary>
        /// Depending on direciton of the turtle rotate anti-clockwise by 90 degrees
        /// </summary>
        private void RotateLeft()
        {
            switch (_CurrentDirection)
            {
                case Direction.N:
                    _CurrentDirection = Direction.W;
                    break;

                case Direction.S:
                    _CurrentDirection = Direction.E;
                    break;

                case Direction.W:
                    _CurrentDirection = Direction.S;
                    break;

                case Direction.E:
                    _CurrentDirection = Direction.N;
                    break;
            }
        }

        /// <summary>
        /// Depending on direciton of the turtle rotate clockwise by 90 degrees
        /// </summary>
        private void RotateRight()
        {
            switch (_CurrentDirection)
            {
                case Direction.N:
                    _CurrentDirection = Direction.E;
                    break;

                case Direction.S:
                    _CurrentDirection = Direction.W;
                    break;

                case Direction.W:
                    _CurrentDirection = Direction.N;
                    break;

                case Direction.E:
                    _CurrentDirection = Direction.S;
                    break;
            }
        }

        /// <summary>
        /// Set the turtle position to the specified inital position
        /// </summary>
        private void ResetTurtle()
        {
            this._CurrentPosX = _OriginalPosX;
            this._CurrentPosY = _OriginalPosY;
            _CurrentDirection = _OriginalDirection;
            Common.Utils.PrintDebugInformation("Turtle position reset");
        }
	}
}
