﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using TurtleSolution.Common;

using Direction = TurtleSolution.Common.DataTypes.Direction;
using Command = TurtleSolution.Common.DataTypes.Command;
using CommandResult = TurtleSolution.Common.DataTypes.CommandResult;


namespace TurtleSolution.SolutionObjects
{

    /// <summary>
    /// General application that extends the Application abstract class 
    /// </summary>
    public class TurtleApplication : Application
    {
        /// <summary>
        /// Inputted filepath
        /// </summary>
        private String _FilePath;
        public String FilePath
        {
            get { return _FilePath; }
        }

        /// <summary>
        /// World instance
        /// </summary>
        private World _TurtleWorld;
        public World TurtleWorld
        {
            get { return _TurtleWorld; }
        }

        /// <summary>
        /// Turtle instance
        /// </summary>
        private Turtle _MyTurtle;
        public Turtle MyTurtle
        {
            get { return MyTurtle; }
        }

        /// <summary>
        /// Location of required information from the file 
        /// </summary>
        private const int BOARD_SIZE_FROM_FILE = 0;
        private const int MINE_LOCATION_FROM_FILE = 1;
        private const int EXIT_LOCATION_FROM_FILE = 2;
        private const int TURTLE_INITIAL_POSITION_FROM_FILE = 3;
        private const int TURTLE_PATHS_STARTING_INDEX = 4;

        private const int COMMAND_LINE_FILE_PATH_POSITION = 0;

        public TurtleApplication(string[] commandLineArguments) : base(commandLineArguments)
        {}

        /// <summary>
        /// Handles the inputted information through the commandline 
        /// For the purpose of this assignment no need for a map
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        protected override bool ParseCommandLineArguments(string[] args)
        {
            // if arguments inputted to application
            if(args.Length >= 1)
            {
                string currentFilePath = args[COMMAND_LINE_FILE_PATH_POSITION];
                Common.Utils.PrintDebugInformation("Parsed commandline input: "+currentFilePath);
                if (File.Exists(currentFilePath))
                {
                    this._FilePath = currentFilePath;
                    return true;
                }
                else
                {
                    throw new FileNotFoundException("Inputted filepath is not properly set");
                }
            }
            // Else throw exception
            else
            {
                throw new Exception("File path was not inputted to application");
            }
        }

        /// <summary>
        /// Converts the details for the exit location specified in the file 
        /// into a Tuple with the items representing the x and y coordinates
        /// </summary>
        /// <param name="details"></param>
        /// <param name="exitLocation"></param>
        private Tuple<int, int> ProcessExitLocation(LinkedList<string> details)
        {
            return Common.Utils.ParseIntoIntTuple(details.ElementAt(0), details.ElementAt(1), "Failed to parse board size");
        }

        /// <summary>
        /// Converts the world dimensions from string into a tuple of width and height
        /// </summary>
        /// <param name="details"></param>
        /// <param name="boardDim"></param>
        private Tuple<int, int> ProcessBoardDimensions(LinkedList<string> details)
        {
            return Common.Utils.ParseIntoIntTuple(details.ElementAt(0), details.ElementAt(1), "Failed to parse board size");
        }

        /// <summary>
        /// Convert a string list of minelocations into a list of int coordinates 
        /// </summary>
        /// <param name="mineLocations"></param>
        /// <param name="processedMineLocations"></param>
        private LinkedList<Tuple<int, int>> ProcessMines(LinkedList<string> mineLocations)
        {
            LinkedList<Tuple<int, int>> processedMineLocations = new LinkedList<Tuple<int, int>>();
            foreach (string currentMineLocation in mineLocations)
            {
                //split the components and remove the ','
                string[] mineLocationComponents = currentMineLocation.Split(new char[] { ',' });
                //check if left with two coordinate components
                if (mineLocationComponents.Length == 2)
                {
                    Tuple<int, int> location = Common.Utils.ParseIntoIntTuple(mineLocationComponents[0], mineLocationComponents[1], "Failed to parse mine locations");
                    processedMineLocations.AddLast(location);
                }
            }
            return processedMineLocations;
        }

        /// <summary>
        /// Converts the inital string status of the turtle into a respective Direction and Coordinates
        /// </summary>
        /// <param name="details"></param>
        /// <param name="initialDirection"></param>
        /// <param name="initialPosition"></param>
        private void ProcessInitalTurtleStatus(LinkedList<string> details, out Direction initialDirection, out Tuple<int, int> initialPosition)
        {
            initialDirection = Direction.N;
            if (details.Count == 3)
            {
                initialPosition = Common.Utils.ParseIntoIntTuple(details.ElementAt(0), details.ElementAt(1), "Failed to parse intial postion");
                char tempInitialDirection = '0';
                if (char.TryParse(details.ElementAt(2), out tempInitialDirection))
                    initialDirection = Common.Utils.ParseDirection(tempInitialDirection);
            }
            else
            {
                throw new Exception("Failed to parse the initial turtle location");
            }
        }

        /// <summary>
        /// Convert the string representation of paths into a queue of enum commands
        /// </summary>
        /// <param name="stringifiedTurtlePaths"></param>
        /// <param name="processedPaths"></param>
        private LinkedList<Queue<Command>> ProcessTurtlePaths(LinkedList<LinkedList<string>> stringifiedTurtlePaths)
        {
            return Common.Utils.ProcessPaths(stringifiedTurtlePaths);
        }

        /// <summary>
        /// Initialises the various components of the application
        /// </summary>
        protected override void Initialise()
        {
            //read file content
            LinkedList<LinkedList<string>> fileContent = Common.Utils.ReadFile(_FilePath);

            // Check that the file contains more than 5 lines of information
            if (fileContent.Count > 5)
            {
                // set temporary variables with the content of the read file
                LinkedList<string> boardSize = fileContent.ElementAt(BOARD_SIZE_FROM_FILE);
                LinkedList<string> mineLocations = fileContent.ElementAt(MINE_LOCATION_FROM_FILE);
                LinkedList<string> exitLocation = fileContent.ElementAt(EXIT_LOCATION_FROM_FILE);
                LinkedList<string> initialTurtleDetails = fileContent.ElementAt(TURTLE_INITIAL_POSITION_FROM_FILE);
                LinkedList<LinkedList<string>> turtlePaths = new LinkedList<LinkedList<string>>();

                LinkedList<Tuple<int, int>> processedMineLocations = new LinkedList<Tuple<int, int>>();
                LinkedList<Queue<char>> processedTurtlePaths = new LinkedList<Queue<char>>();
                Tuple<int, int> processedExitLocation;
                Tuple<int, int> processedBoardDim;
                Direction initialTurtleDirection = Direction.N;
                Tuple<int, int> initialTurtlePosition;
                LinkedList<Queue<Command>> processedPaths = new LinkedList<Queue<Command>>();

                //line 1 - board size 
                processedBoardDim = ProcessBoardDimensions(boardSize);
                Common.Utils.PrintDebugInformation("Parsed board size infomrmation succesfully"+processedBoardDim.Item1+"&"+processedBoardDim.Item2);

                // Line 2 - Mine Location
                processedMineLocations = ProcessMines(mineLocations);
                Common.Utils.PrintDebugInformation("Parsed mines succesfully, total of " + processedMineLocations.Count+" mines");

                // Line 3 - Exit Location
                processedExitLocation = ProcessExitLocation(exitLocation);
                Common.Utils.PrintDebugInformation("Parsed exit location succesfully, location"+processedExitLocation.Item1 +" "+processedExitLocation.Item2);

                // Line 4 - Turtle location and initial directions
                ProcessInitalTurtleStatus(initialTurtleDetails, out initialTurtleDirection, out initialTurtlePosition);
                Common.Utils.PrintDebugInformation("Parsed initial turtle direction, " + initialTurtleDirection.ToString());

                // Line 5  - All the paths to be tested
                for (int index = TURTLE_PATHS_STARTING_INDEX; index < fileContent.Count; index++)
                {
                    turtlePaths.AddLast(fileContent.ElementAt(index));
                }
                Common.Utils.PrintDebugInformation("Added a total of , " + turtlePaths.Count+" turtle paths ");

                processedPaths =  ProcessTurtlePaths(turtlePaths);

                processedPaths =  Common.Utils.ProcessPaths(turtlePaths);
                Common.Utils.PrintDebugInformation("Processed " + processedPaths.Count + " turtle paths ");

                //instansiate components
                this._TurtleWorld = new World(processedBoardDim, processedMineLocations, processedExitLocation);
                this._MyTurtle = new Turtle(initialTurtlePosition, initialTurtleDirection, _TurtleWorld, processedPaths);

                this._TurtleWorld.InitialiseWorld();
                this._MyTurtle.InitialiseTurtle();
            }
            else
            {
                //if not throw error
                throw new Exception("File does not contain all the information");
            }
        }

        /// <summary>
        /// The execute methos will run the turtles logic to check the inputted paths
        /// </summary>
        protected override void Execute()
        {
            _MyTurtle.CheckPaths();
        }
    }
}
