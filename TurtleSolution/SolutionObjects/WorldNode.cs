using System;
using TurtleSolution.Interfaces;

namespace TurtleSolution.SolutionObjects
{
    /// <summary>
    /// This enum describes the type of node 
    /// </summary>
    public enum NodeType
    {
        MINE,
        EXIT,
        NORM
    }

    /// <summary>
    /// The WorldNode describes a single node in the World
    /// </summary>
    public class WorldNode
    {
        private NodeType _Type;
        public NodeType Type
        {
            get { return _Type; }
        }

        private int _NodeID;
        public int NodeID
        {
            get { return _NodeID; }
        }

        public WorldNode(NodeType type, int index)
        {
            this._Type = type;
            this._NodeID = index;
        }
    }
}
