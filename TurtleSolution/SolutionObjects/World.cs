using System;
using System.Collections.Generic;

using TurtleSolution.Interfaces;

using Position = System.Tuple<int, int>;
using Size2D = System.Tuple<int, int>;

namespace TurtleSolution.SolutionObjects
{
    /// <summary>
    /// The world manages all the nodes, including the mines and exist
    /// </summary>
    public class World : IWorld
    {
        /// <summary>
        /// A tuple of the dimensions of the board
        /// </summary>
        private Size2D _BoardSize;
        public Tuple<int, int> BoardSize
        {
            get { return _BoardSize; }
        }

        /// <summary>
        /// List of all mines
        /// </summary>
        private LinkedList<Position> _Mines;
        public LinkedList<Position> Mines
        {
            get { return _Mines; }
        }

        /// <summary>
        /// Exit point of the board
        /// </summary>
        private Position _Exist;
        public Position Exit
        {
            get { return _Exist; }
        }

        /// <summary>
        /// An array containing all the nodes of the world
        /// the nodes are stored in a linear array for better cache coherency 
        /// </summary>
        private WorldNode[] _WorldNodes;
        public WorldNode[] WorldNodes
        {
            get { return _WorldNodes; }
        }


        /// <summary>
        ///  Constructor
        /// </summary>
        /// <param name="boardSize"></param>
        /// <param name="mines"></param>
        /// <param name="exist"></param>
        public World(Size2D boardSize , LinkedList<Position> mines, Position exist)
        {
            this._BoardSize = boardSize;
            this._Mines = mines;
            this._Exist = exist;
        }

        /// <summary>
        /// Initialises all the nodes of the board including the mines and the exit
        /// </summary>
        public void InitialiseWorld()
        {
            int listLocation = 0;
            // index of the exit position
            int exitPosition = 0;
            //total amount of nodes in the world
            int totalAmountNodes = this._BoardSize.Item1 * this._BoardSize.Item2;
            this._WorldNodes = new WorldNode[totalAmountNodes];

            // Hashset conataining all indices that will be set as mines
            HashSet<int> minesPosition = new HashSet<int>();

            // Loop through all the mines, get the index position and store them in the hashset
            foreach(Position minePositon in _Mines)
            {
                listLocation = minePositon.Item2 * _BoardSize.Item1 + minePositon.Item1;
                minesPosition.Add(listLocation);
            }

            // determine the index of the exitPosition
            exitPosition = _Exist.Item2 * _BoardSize.Item1 + _Exist.Item1;

            // Initialise all nodes of the board and set the mines and exit accordingly
            for (int nodeIndex = 0; nodeIndex < totalAmountNodes; nodeIndex++)
            {
                if(minesPosition.Contains(nodeIndex))
                    this._WorldNodes[nodeIndex] = new WorldNode(NodeType.MINE, nodeIndex);
                else if(nodeIndex == exitPosition)
                    this._WorldNodes[nodeIndex] = new WorldNode(NodeType.EXIT, nodeIndex);
                else
                    this._WorldNodes[nodeIndex] = new WorldNode(NodeType.NORM, nodeIndex);
            }

            Common.Utils.PrintDebugInformation("World Initialisation completed");
        }

        /// <summary>
        /// Gets the required node according to the x and y coordinate
        /// </summary>
        /// <param name="x"> x-axis coordinate of the node</param>
        /// <param name="y"> y-axis coordinate of the node</param>
        /// <returns>The node at that location</returns>
        public WorldNode GetNode(int x, int y)
        {
            // linearise the 2D coordinate
            int nodeLocation = y * _BoardSize.Item1 + x;

            // If the index is smaller than the limit of the array
            //return the node
            if(nodeLocation < WorldNodes.Length)
            {
                return this._WorldNodes[nodeLocation];
            }
            //else fire an exception
            else
            {
                throw new Exception("Requested node is out of board bounds");
            }
        }
    }
}
